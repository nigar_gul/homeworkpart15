package Task6;

import java.util.HashMap;
import java.util.Map;

public class Student {
    public Map<String, Integer> students;

    public Student() {
        students = new HashMap<>();
    }

    public void addStudent( int id, String name) {
        students.put(name,id);
        double gpa = 0;
        System.out.println("Student added: " +name+ "-" +gpa+ "-"+id);
    }

    public void removeStudent(int id) {
        if (students.containsKey(id)) {
            students.remove(id);
            System.out.println("Student removed from the list");
        } else {
            System.out.println("Student not found: " + id);
        }
    }

    public void retrieveName(int id) {
        Integer name = students.get(id);
        double gpa = 0;
        if (name != null) {
            System.out.println("Student found:" + name + " - " + id + " - " + gpa);
        } else {
            System.out.println("Student with ID " + id + " does not exist in the list.");
        }
    }
        public void printStudents() {
            if (students.isEmpty()) {
                System.out.println("No student in the list.");
            } else {
                System.out.println("List of students:" + students.entrySet());
            }
        }
    }
