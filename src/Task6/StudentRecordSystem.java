package Task6;

import java.util.Scanner;

class StudentRecordSystem {
    public static void main(String[] args) {

        Student student = new Student();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Enter the option number:");
            System.out.println("1. Add a student");
            System.out.println("2. Delete a student");
            System.out.println("3. Retrieve a student");
            System.out.println("4. Display all students");
            System.out.println("5. Exit");

        int option = scanner.nextInt();
        scanner.nextLine();

        switch (option){
            case 1:
                System.out.println("Enter student name:");
                String addName = scanner.nextLine();
                System.out.println("Enter student ID:");
                int addId = scanner.nextInt();
                System.out.println("Enter student GPA:");
                double gpa = scanner.nextDouble();
                student.addStudent(addId, addName);
                break;

            case 2:
                System.out.println("Enter student id to delete");
                int deleteId = scanner.nextInt();
                student.removeStudent(deleteId);
                break;

            case 3:
                System.out.println("Enter student id to retrieve");
                int retrieveId = scanner.nextInt();
                student.retrieveName(retrieveId);
                break;

            case 4:
                student.printStudents();
                break;

            case 5:
                System.out.println("Exiting the students' list..");
                System.exit(0);
            default:
                System.out.println("Invalid option. Please try again.");
                break;
        }
        }
    }
}
