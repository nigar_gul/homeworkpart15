package Task5;

import java.util.HashMap;
import java.util.Map;

public class ToysInventory {
    public static void main(String[] args) {

        // Creating a HashMap to store inventory items and their quantities.
        //Adding an item to the inventory with an initial quantity:

        Map<String, Integer> toys = new HashMap<>();
        toys.put("doll", 62);
        toys.put("bicycle", 65);
        toys.put("car", 78);
        toys.put("teddy bear", 49);
        toys.put("ball", 50);

        System.out.println("------------------------------------------------");
        System.out.println("Toys' keys and their values:" + toys.entrySet());
        System.out.println("------------------------------------------------");

        //Updating the quantity of an existing item in the inventory:

        toys.computeIfPresent("teddy bear", (key, oldValue) -> oldValue + 17);
        toys.computeIfPresent("bicycle", (key, oldValue) -> oldValue - 20);
        System.out.println("Updated values are: " + toys);
        System.out.println("------------------------------------------------");

//        Removing an item from the inventory:

        toys.remove("doll");
        System.out.println("Toys list after removing: " + toys);
        System.out.println("------------------------------------------------");

//      Checking if an item exists in the inventory:

        if (toys.containsKey("doll")) {
            System.out.println("There is a toy like doll");
        } else {
            System.out.println("There is not a toy like doll");
            System.out.println("------------------------------------------------");

            //    Printing all items in the inventory and their quantities.

            System.out.println("Last version of the list:" + toys.entrySet());
        }

    }
}