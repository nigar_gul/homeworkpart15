package Task1;

import java.util.ArrayList;
import java.util.Collections;

public class MaxMinNumbers {
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(12);
        numbers.add(84);
        numbers.add(9);
        numbers.add(99);
        numbers.add(77);
        numbers.add(15);
        numbers.add(52);

        int max = Collections.max(numbers);
            int min = Collections.min(numbers);
            System.out.println("Max value is:" + max);
            System.out.println("Min value is:" + min);

         }
        }

