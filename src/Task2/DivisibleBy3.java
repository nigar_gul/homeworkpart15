package Task2;

import java.util.ArrayList;

public class DivisibleBy3 {
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(12);
        numbers.add(84);
        numbers.add(9);
        numbers.add(99);
        numbers.add(77);
        numbers.add(15);
        numbers.add(52);

        ArrayList<Integer> divisibleBy3 = new ArrayList<>();
        for (Integer number : numbers) {
            if (number % 3 != 0) {
                divisibleBy3.add(number);
            }
        }
        System.out.print("Updated list is: ");
        for (int i = 0; i < divisibleBy3.size(); i++) {
            System.out.print(divisibleBy3.get(i));
            if (i != divisibleBy3.size() - 1) {
                System.out.print(", ");
            }
        }
    }
}