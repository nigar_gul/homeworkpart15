package Task4;


import java.util.ArrayList;
import java.util.Collections;

public class AverageNumbers {
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(12);
        numbers.add(84);
        numbers.add(9);
        numbers.add(99);
        numbers.add(77);
        numbers.add(15);
        numbers.add(52);

        ArrayList<Integer> averageOfEven =new ArrayList<> ();

        int sum=0;
        int count=0;
        for (int number: numbers) {
            if (number % 2 == 0) {
                sum += number;
                count++;
            }
        }
        int average = (int) sum / count;

        System.out.println("Average of all even numbers is: " +average);
    }
}

