package Task3;

import java.util.ArrayList;

public class Names {
    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<>();
        names.add("Nigar");
        names.add("Banu");
        names.add("Elvin");
        names.add("Chingiz");
        if (names.contains("Nigar")) {
            System.out.println("There is a name like Nigar.");
        } else {
            System.out.println("There is not a name like Nigar.");
        }
    }
}